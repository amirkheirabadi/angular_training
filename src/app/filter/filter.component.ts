import { Component, OnInit, Input, HostListener } from '@angular/core';
import { DisplayComponent } from '../display/display.component';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})
export class FilterComponent implements OnInit {
  ngOnInit() {}

  @Input() listComponent: DisplayComponent;

  @HostListener('click')
  changeFilter(value) {
    this.listComponent.filterItems(value);
  }
}
