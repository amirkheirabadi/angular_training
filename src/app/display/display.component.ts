import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, PageEvent} from '@angular/material';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
})
export class DisplayComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  ngOnInit() {
    this.refresh()
    this.dataSource.paginator = this.paginator;
  }
  
  data = [
    {
      name: 'amir',
      score: '10',
    },
    {
      name: 'ali',
      score: '20',
    },
    {
      name: 'amir',
      score: '10',
    },
    {
      name: 'ali',
      score: '20',
    },
    {
      name: 'amir',
      score: '10',
    },
    {
      name: 'ali',
      score: '20',
    },
  ];
  filterdData = this.data;
  displayedColumns = ['name', 'score', 'action'];
  dataSource = new MatTableDataSource();
  
  paginationLength = this.filterdData.length

  refresh() {
    this.dataSource.data = this.filterdData
    this.paginationLength = this.filterdData.length
  }

  deleteItem(index) {
    this.filterdData.splice(index, 1);
    this.refresh()
  }

  paginationEventRecive(event) {
    this.dataSource.paginator = event;
  }

  filterItems(term) {
    this.filterdData = this.data.filter(item => {
      return item.name.indexOf(term) > -1;
    });
    this.refresh()
  }
}
