import { Component, OnInit, Input, HostListener, Output, EventEmitter, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Output() paginationEvent: EventEmitter<null> = new EventEmitter();

  @Input()
  length = 1000;

  paginationChangeDetect(event) {
    this.paginationEvent.emit(this.paginator);
  }
}
